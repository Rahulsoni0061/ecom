﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Clothshop.Web.Startup))]
namespace Clothshop.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
