﻿using Clothshop.Entities;
using Clothshop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Clothshop.Web.Controllers
{
    public class ProductController : Controller
    {
        ProductsService productsService = new ProductsService();
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProductTable(string search) 
        {
            var products = productsService.GetProducts();

            if (string.IsNullOrEmpty(search) == false)
            {
                products = products.Where(p => ((p.Name != null) && (p.Name.ToLower().Contains(search.ToLower())))).ToList();

            }
            /*foreach(var p in products)
            {
                if(p.Name == search)
                    found
            }*/

            return PartialView(products);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Create(Product product)
        {
            productsService.SaveProduct(product);

            return RedirectToAction("Index");
        }
    }
}